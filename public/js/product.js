
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }

});
const Product = (function () {
    let modules = {}

    modules.getListBy = function (url, data) {
        Base.callApiNormally(url, data)
            .then(function (res) {
                $('#list').html('').append(res);
            });
    }

    modules.getList = function () {
        let url = $('#list').data('action');
        let data = $('#myForm').serialize();
        Product.getListBy(url, data);
    }

    modules.create = function () {
        let url = $('#addProductForm').data('action');
        let data = new FormData(document.getElementById('addProductForm'));
        CustomAlert.resetError();
        Base.callApiData(url, data, 'POST')
            .then(function (res) {
                $('#modal-form-create').modal('hide');
                // Product.resetError(res.responseJSON.errors);
                Product.resetForm();
                Product.getListBy($('#list').data('action'));
                CustomAlert.messageSuccessModal(res.message);
            })
            .fail(function (res){
                // ?.
                console.log(res)
                CustomAlert.showError(JSON.parse(res.responseText).errors);
            });
    }

    modules.show = function (url) {
        Base.callApiNormally(url)
            .then(function (res) {
                $('#edit').html('').append(res);
            })
            .fail(function () {
                $('#modal-form-edit').modal('hide');
                CustomAlert.showModalError();
            })
    }

    modules.showDetail = function (url) {
        Base.callApiNormally(url)
            .then(function (res) {
                $('#show').html('').append(res);
            })
            .fail(function () {
                $('#modal-form-show').modal('hide');
                CustomAlert.showModalError();
            })
    }

    modules.update = function () {
        let url = $('#updateProductForm').data('action');
        let data = new FormData(document.getElementById('updateProductForm'));
        CustomAlert.resetError();
        Base.callApiData(url, data, 'post')
            .done(function (res) {
                // $('#modal-form-update').modal('hides');
                Product.resetForm();
                $('#modal-form-update').hide();
                $('.modal-backdrop').remove();
                CustomAlert.messageSuccessModal(res.message);
                Product.getListBy($('#list').data('action'));
            })
            .fail(function (res){
                console.log(res)
                CustomAlert.showError(JSON.parse(res.responseText).errors);
            });
    }

    modules.delete = function (element) {
        let url = element.data('action');
        let name = element.data('name-show');
        Base.confirmDelete(name).then(
            function () {
                Base.callApiNormally(url, {}, 'DELETE')
                    .then(function () {
                        Product.getListBy($('#list').data('action'));
                        CustomAlert.messageSuccessModal("Delete product success!");
                    })
                    .fail(function () {
                        CustomAlert.showModalError();
                    });
            }
        )
    }

    modules.resetForm = function () {
        $('.selectpicker').val(null).trigger("change");
        $('#addProductForm').trigger('reset');
        $(".show-image").attr("src", "");
    }

    // modules.resetError = function (obj){
    //     CustomAlert.resetError(obj);
    // }

    modules.pageLink = function (element) {
        let url = element.attr('href');
        Product.getListBy(url);
    }

    modules.edit = function (element) {
        let url = element.data('action');
        Product.show(url);
    }

    modules.showModalDetail = function (element) {
        let url = element.data('action');
        Product.showDetail(url);
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {

    Product.getList($('#list').data('action'));

    $('#myForm').on('click', function (e) {
        e.preventDefault();
        CustomAlert.resetError();
        Product.getList();
    })

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Product.pageLink($(this));
    });

    $(document).on('click', '#btn-create', function (e) {
        e.preventDefault();
        Product.create();
    });

    $(document).on('click', '#btn-add-new-product', function (e) {
        Image.deleteImage();
    });

    $(document).on('change', '.image', function () {
        Image.showImage($(this));
    });

    $(document).on('click', '.btn-delete-image', function (e) {
        e.preventDefault();
        Image.deleteImage();
    });

    $(document).on('click', '.btn-delete-product', function (e) {
        e.preventDefault();
        Product.delete($(this));
    });

    $(document).on('click', '.btn-edit-product', function (e) {
        Product.edit($(this));
    });

    $(document).on('click', '#btn-update', function (e) {
        e.preventDefault();
        Product.update();
    });

    $(document).on('click', '.btn-show-product', function (e) {
        Product.showModalDetail($(this));
    });

    $('#category_id').on('change', debounce(function () {
        Product.getList();
    }))

    $('.product_name').on('keyup', debounce(function () {
        Product.getList();
    }))

    $('.min_price, .max_price').on('keyup', debounce(function () {
        Product.getList();
    }))

})

function debounce(func, timeout = 1000) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
