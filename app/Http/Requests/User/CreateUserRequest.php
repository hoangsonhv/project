<?php

namespace App\Http\Requests\User;

use App\Rules\VaidateEmailCreateRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => [
                'bail',
                'required',
                'unique:users',
                new VaidateEmailCreateRule($this->email),
            ],
            'password' => 'required',
            'address' => 'required',
            'phone' => 'required|regex:/(0)[0-9]{9}/'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name cannot be null.',
            'email.max' => 'Emails cannot be longer than 255 characters.',
            'email.required' => 'Email cannot be null.',
            'email.unique' => 'Email already in use.',
            'password.required' => 'Password cannot be null.',
            'address.required' => 'Address cannot be null.',
            'phone.required' => 'Phone cannot be null.',
        ];
    }
}
