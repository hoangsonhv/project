<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected ProductRepository $productRepo;
    protected CategoryRepository $cateRepo;
    protected UserRepository $userRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(ProductRepository $productRepo, UserRepository $userRepo, CategoryRepository $cateRepo)
    {
        $this->productRepo = $productRepo;
        $this->userRepo = $userRepo;
        $this->cateRepo = $cateRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $productCount = $this->productRepo->count();
        $categoryCount = $this->cateRepo->count();
        $userCount = $this->userRepo->count();
        return view('home', compact(['productCount', 'userCount', 'categoryCount']));
    }
}
