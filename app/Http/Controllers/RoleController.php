<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index()
    {
        $listRoles = $this->roleService->all();
        return view("admin.roles.index", compact('listRoles'));
    }

    public function show($id)
    {
        $role = $this->roleService->findById($id);
        return view("admin.roles.show", compact('role'));
    }

    public function create()
    {
        return view("admin.roles.create");
    }

    public function store(CreateRoleRequest $request)
    {
        $this->roleService->store($request);
        return redirect()->route('roles.index')->with('message', "Create role success.");
    }

    public function edit($id)
    {
        $role = $this->roleService->findById($id);
        return view("admin.roles.edit", compact('role'));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route("roles.index")->with("message", "Update role success!");
    }

    public function destroy($id)
    {
        $this->roleService->destroy($id);
        return redirect()->back()->with("message", "Delete role success.");
    }
}
