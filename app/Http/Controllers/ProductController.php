<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        return view("admin.products.index");
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        return view("admin.products.list", compact('products'))->render();
    }

    public function show($id)
    {
        $product = $this->productService->findById($id);
        return view("admin.products.show", compact('product'));
    }

    public function create()
    {
        return view("admin.products.create");
    }

    public function store(CreateProductRequest $request)
    {
        $product = $this->productService->store($request);
        $productResource = new ProductResource($product);
        return $this->sentSuccessResponse($productResource, 'Create product success.', Response::HTTP_CREATED);
    }

    public function edit($id)
    {
        $product = $this->productService->findById($id);
        return view("admin.products.edit", compact('product'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->productService->update($request, $id);
        return $this->sentSuccessResponse($product, 'Update product success.', Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $this->productService->destroy($id);
        return $this->sentSuccessResponse('', 'Delete product success.', Response::HTTP_OK);
    }
}
