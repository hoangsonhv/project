<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view("admin.users.index", compact('users'));
    }

    public function show($id)
    {
        $user = $this->userService->findById($id);
        return view("admin.users.show", compact('user'));
    }

    public function create()
    {
        return view("admin.users.create");
    }

    public function store(CreateUserRequest $request)
    {
        $this->userService->store($request);
        return redirect()->route('users.index')->with("message", "Create user success.");
    }

    public function edit($id)
    {
        $user = $this->userService->findById($id);
        return view("admin.users.edit", compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route("users.index")->with("message", "Update user success.");
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect()->back()->with('message', "Delete user success.");
    }
}
