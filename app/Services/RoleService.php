<?php

namespace App\Services;

use App\Http\Requests\Role\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->latest('id')->paginate(5);
    }

    public function findById($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission_id'] = $request->permission_id ?? [];
        $dataCreate['name'] = str_slug($request->name);
        $role = $this->roleRepository->create($dataCreate);
        $role->attachPermission($dataCreate['permission_id']);
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['permission_id'] = $request->permission_id ?? [];
        $role->update($dataUpdate);
        $role->syncPermission($dataUpdate['permission_id']);
        return $role;
    }

    public function destroy($id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $role->detachPermission();
        $role->delete();
        return $role;
    }
}
