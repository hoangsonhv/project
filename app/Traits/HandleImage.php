<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected string $path = 'upload/images/products/';
    protected string $imageDefault = 'default.jpg';

    public function veryFileImage($request)
    {
        return $request->hasFile('image');
    }

    public function saveImage($request)
    {
        if ($this->veryFileImage($request)) {
            $file = $request->file('image');
            $fileName = time() . $file->getClientOriginalName(); // name file
            Image::make($file)->resize(300, 400)->save($this->path . $fileName);
            return $fileName;
        }
        return $this->imageDefault;
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->veryFileImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $pathName = $this->path . $imageName;
        if (file_exists($pathName) && $imageName != $this->imageDefault) {
            unlink($pathName);
        }
    }
}
