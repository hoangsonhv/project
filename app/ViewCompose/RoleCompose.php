<?php

namespace App\ViewCompose;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\View\View;

class RoleCompose{

    protected $roles;
    protected $permissions;

    public function __construct(Role $roles, Permission $permissions)
    {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    public function compose(View $view)
    {
        $view->with('roles', $this->roles->all());
        $view->with('permissions', $this->permissions->all());
        $view->with('group_permission', $this->permissions->all()->groupBy('group_name'));
    }
}
