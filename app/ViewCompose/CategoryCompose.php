<?php

namespace App\ViewCompose;

use App\Models\Category;
use Illuminate\View\View;

class CategoryCompose{

    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function compose(View $view)
    {
        $view->with('categories', $this->category->all());
    }
}
