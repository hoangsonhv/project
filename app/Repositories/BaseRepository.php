<?php

namespace App\Repositories;

abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        $this->model = app()->make($this->model());
    }

    public function all()
    {
        return $this->model->paginate(10);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function latest($id)
    {
        return $this->model->latest('id');
    }
}
