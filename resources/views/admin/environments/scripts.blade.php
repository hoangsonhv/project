<script src="{{ asset("assets/js/vendor.min.js") }}"></script>
<script src="{{ asset("assets/js/app.min.js") }}"></script>
<script src="{{ asset("assets/js/vendor/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("assets/js/vendor/dataTables.bootstrap5.js") }}"></script>
<script src="{{ asset("assets/js/vendor/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("assets/js/vendor/responsive.bootstrap5.min.js") }}"></script>
<script src="{{ asset("assets/js/vendor/dataTables.checkboxes.min.js") }}"></script>
<script src="{{ asset("assets/js/pages/demo.products.js") }}"></script>
<script src="{{ asset("assets/js/style.js") }}"></script>
<script src="{{ asset("js/base.js") }}"></script>
{{--<script src="{{asset('js/product.js')}}"></script>--}}
{{--<script src="{{asset('js/user.js')}}"></script>--}}
{{--<script src="{{asset('js/category.js')}}"></script>--}}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset("https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js") }}"></script>
<script src="{{ asset("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.14/js/bootstrap-select.min.js") }}"></script>

{{--<script src="{{ asset("//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js") }}"></script>--}}
<script>
    $('#all').change(function(e) {
        if (e.currentTarget.checked) {
            $('.rows').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $('.rows').find('input[type="checkbox"]').prop('checked', false);
        }
    });

    $('.select-item').on('change', function () {
        // console.log($(`.select-item:checked`).length)
        if($(`.select-item:checked`).length === $(`.select-item`).length){
            $('#all').prop('checked', true);
        }else{
            $('#all').prop('checked', false);
        }
    });
</script>
