<div class="leftside-menu">
    <a href="{{ route("home") }}" class="logo text-center logo-light">
        <img src="{{ asset("assets/images/text.png") }}" alt="" height="60">
    </a>
    <div class="h-100" id="leftside-menu-container" data-simplebar="">
        <ul class="side-nav">
            @hasPermission('user_list')
            <li class="side-nav-item">
                <a href="{{ url("users") }}" class="side-nav-link">
                    <i class="dripicons-user-group"></i>
                    <span> User </span>
                </a>
            </li>
            @endhasPermission
            @hasPermission('role_list')
            <li class="side-nav-item">
                <a href="{{ url("roles") }}" class="side-nav-link">
                    <i class="dripicons-align-right"></i>
                    <span> Role </span>
                </a>
            </li>
            @endhasPermission
            @hasPermission('product_list')
            <li class="side-nav-item">
                <a href="{{ url("products") }}" class="side-nav-link">
                    <i class="uil-store"></i>
                    <span> Product </span>
                </a>
            </li>
            @endhasPermission
            @hasPermission('category_list')
            <li class="side-nav-item">
                <a href="{{ url("categories") }}" class="side-nav-link">
                    <i class="dripicons-briefcase"></i>
                    <span> Category </span>
                </a>
            </li>
            @endhasPermission
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
