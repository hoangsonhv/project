@extends("admin.layouts.layouts")
@section("content")
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Role</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                @hasPermission('role_create')
                                <a href="{{ route("roles.create") }}">
                                    <button type="button" class="btn btn-danger mb-2">
                                        <i class="mdi mdi-plus-circle me-2"></i>Add Role
                                    </button>
                                </a>
                                @endhasPermission
                            </div>
                            <!-- Modal -->
                        </div>
                        <div class="table-responsive">
                            <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
                                <thead class="table-light">
                                <tr style="white-space: nowrap;">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Display Name</th>
                                    <th>Permission</th>
                                    <th style="width: 85px;text-align: center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($listRoles) != 0)
                                @foreach($listRoles as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->display_name }}</td>
                                        <td>
                                            @if($item->name == "super-admin")
                                                <span class="badge bg-success">All</span>
                                            @else
                                                @foreach($item->permissions as $permission)
                                                    <span class="badge bg-success">{{ $permission->display_name }}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                           <div class="container">
                                               <div class="flex-class justify-content-center">
                                                   <div class="col-sm-4">
                                                       <a href="{{ route('roles.show', $item->id) }}"> <button type="button" class="btn btn-primary"><i class="dripicons-information"></i></button></a>
                                                   </div>
                                                   @if($item->name != "super-admin")
                                                   @hasPermission('role_edit')
                                                   <div class="col-sm-4">
                                                       <a href="{{ route('roles.edit', $item->id) }}"> <button type="button" class="btn btn-secondary">  <i class="dripicons-document-edit"></i></button></a>
                                                   </div>
                                                   @endhasPermission
                                                   @hasPermission('role_delete')
                                                       <div class="col-sm-4">
                                                           <form id="delete-{{$item->name}}-{{$item->id}}" action="{{ route('roles.destroy',$item->id) }}" method="POST">
                                                               @csrf
                                                               @method('DELETE')
                                                           </form>
                                                           <button data-id="{{$item->id}}"
                                                                   data-name="{{ $item->name }}"
                                                                   data-name-show="{{ $item->display_name }}"
                                                                   type="submit" class="btn btn-danger btn-delete">
                                                               <i class="dripicons-trash"></i>
                                                           </button>
                                                       </div>
                                                   @endif
                                                   @endhasPermission
                                               </div>
                                           </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                @else
                                    <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center"><span style="font-size: 25px; color: #d8d8d8">No data...</span></td>
                                    </tr>
                                    </tbody>
                                @endif
                            </table>
                            {{ $listRoles->appends(request()->all())->links() }}
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
    @push('scripts')
        <script src="{{asset('js/role.js')}}"></script>
    @endpush
@endsection
