@extends('admin.layouts.layouts')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Update Role</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('roles.update',$role->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="modal-body">
                                @include('admin.layouts.form-errors')
                                <div class="mb-3">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" value="{{ $role->name }}" placeholder="Name">
                                </div>
                                <div class="mb-3">
                                    <label>Display Name</label>
                                    <input type="text" name="display_name" class="form-control" value="{{ $role->display_name }}" placeholder="Display name">
                                </div>
                                @if($role->name != "super-admin")
                                    <div class="mb-3 row">
                                        <label>Permission</label>
                                        <div class="checkbox select-all col-sm-1">
                                            <input id="all" type="checkbox" />
                                            <label for="all">Select all</label>
                                        </div>
                                        @foreach($group_permission as $group => $values)
                                            <div class="col-sm-2">
                                                {{ $group }}
                                                @foreach($values as $value)
                                                    <div class="checkbox rows">
                                                        <input id="box-{{ $value->id }}" class="select-item" name="permission_id[]" value="{{ $value->id }}" type="checkbox"
                                                            {{$role->permissions->contains('id',$value->id) ? 'checked' : ''}}/>
                                                        <label for="box-{{ $value->id }}">{{ $value->display_name }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="modal-footer">
                                <a type="button" href="{{ url()->previous() }}" class="btn btn-secondary btn-button">Back</a>
                                <button type="submit" class="btn btn-primary btn-button">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
