@if(session()->has('message'))
    <div class="alert alert-success" style="margin-top: 10px">
        {{ session()->get('message') }}
    </div>
@endif
