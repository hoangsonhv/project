<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.environments.head')
        @stack('links')
    </head>
    <body class="loading" data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <div class="wrapper">
        @include('admin.environments.menu')
        <div class="content-page">
            <div class="content">
                @include('admin.environments.nav-bar')
                @include('admin.layouts.form-success')
                @yield("content")

            </div>
            @include('admin.environments.footer')
        </div>
    </div>
    @include('admin.environments.setting-bar')
    @include('admin.environments.scripts')
    @stack("scripts")
    </body>
</html>
