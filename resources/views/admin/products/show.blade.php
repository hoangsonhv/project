<div class="row">
    <div class="col-md-12" style="margin: auto">
        <div class="card">
            <div class="card-body">
                <div class="detail-product">
                    <dl class="row">
                        <dt class="col-sm-4 text-end">Id :</dt>
                        <dd class="col-sm-8 text-center">{{ $product->id }}</dd>
                        <dt class="col-sm-4 text-end">Name :</dt>
                        <dd class="col-sm-8 text-center">{{ $product->name }}</dd>
                        <dt class="col-sm-4 text-end">Price :</dt>
                        <dd class="col-sm-8 text-center">{{ $product->price }}</dd>
                        <dt class="col-sm-4 text-end">Quantity :</dt>
                        <dd class="col-sm-8 text-center">{{ $product->quantity }}</dd>
                        <dt class="col-sm-4 text-end">Description :</dt>
                        <dd class="col-sm-8 text-center">{{ $product->description }}</dd>
                        <dt class="col-sm-4 text-end">Image :</dt>
                        <dd class="col-sm-8 text-center">
                            <img src="{{ $product->image }}" style="width: 40%" alt="">
                        </dd>
                        <dt class="col-sm-4 text-end">Category :</dt>
                        <dd class="col-sm-8 text-center">
                            @foreach($product->categories as $item)
                                <div class="col-sm text-center">
                                    <span class="badge bg-success">{{ $item->name }}</span>
                                </div>
                            @endforeach
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <hr>
        <div class="mb-3 text-center">
            <button class="btn btn-rounded btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        </div>
    </div>
</div>
