<div class="table-responsive">
    <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
        <thead class="table-light">
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Price</th>
            <th style="width: 85px;text-align: center">Action</th>
        </tr>
        </thead>
        @if(count($products) != 0)
            <tbody>
            @foreach($products as $item)
                <tr>
                    <td class="text-center">{{ $item->id }}</td>
                    <td style="width: 10%"><img src="{{ $item->image }}" style="width: 100%;height: 100px" alt=""></td>
                    <td>{{ $item->name }}</td>
                    <td>{{ number_format($item->price) }} VND</td>
                    <td>
                        <div class="container">
                            <div class="flex-class justify-content-center">
                                @hasPermission('product_show')
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-show-product"
                                            data-action="{{ route('products.show', $item->id) }}"
                                            data-bs-target="#modal-form-show"
                                            data-bs-toggle="modal">
                                        <i class="dripicons-information"></i>
                                    </button>
                                </div>
                                @endhasPermission
                                @hasPermission('product_edit')
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-secondary btn-edit-product"
                                            data-action="{{ route('products.edit', $item->id) }}"
                                            data-bs-target="#modal-form-update"
                                            data-bs-toggle="modal">
                                        <i class="dripicons-document-edit"></i>
                                    </button>
                                </div>
                                @endhasPermission
                                @hasPermission('product_delete')
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-danger btn-delete-product"
                                            id="btn_delete"
                                            data-id="{{ $item->id }}"
                                            data-name-show="{{ $item->name }}"
                                            data-action="{{ route('products.destroy',$item->id) }}">
                                        <i class="dripicons-trash"></i>
                                    </button>
                                </div>
                                @endhasPermission
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        @else
            <tbody>
                <tr>
                    <td colspan="5" class="text-center"><span style="font-size: 25px; color: #d8d8d8">No data...</span></td>
                </tr>
            </tbody>
        @endif
    </table>
    {{ $products->appends(request()->all())->links() }}
</div>
