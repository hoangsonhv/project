@extends("admin.layouts.layouts")
@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Product</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-2">
                                @hasPermission('product_create')
                                <button type="button" class="btn btn-danger mb-2" id="btn-add-new-product"
                                        data-bs-toggle="modal" data-bs-target="#modal-form-create">
                                    <i class="mdi mdi-plus-circle me-2"></i>
                                    Add Product
                                </button>
                                @endhasPermission
                            </div>
                            <div class=" col-md-10 set-search">
                                <form id="myForm" method="get">
                                    <input name="name" class="form-control product_name" value="{{ request()->name ?? '' }}"
                                           style="width: 20%" placeholder="Enter name...">
                                    <select id="category_id" name="category_ids" class="form-control"
                                            style="width: 10%; margin-right: 5px;margin-left: 5px">
                                        <option value=""><span style="color: #ada8a8">Category</span></option>
                                        @foreach($categories as $category)
                                            <option
                                                {{ $category->id == request('category_ids') ? 'selected' : '' }} value="{{ $category->id }}">
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input name="min_price" type="number" class="form-control min_price" style="width: 12%"
                                           value="{{ request()->min_price ?? '' }}" placeholder="Enter min price..">
                                    <input name="max_price" type="number" class="form-control max_price" style="width: 12%"
                                           value="{{ request()->max_price ?? '' }}" placeholder="Enter max price..">
{{--                                    <button type="submit" class="btn btn-primary" style="margin-left: 5px">Search--}}
{{--                                    </button>--}}
                                </form>
                            </div>
                        </div>
                        <div id="list" data-action="{{ route('products.list') }}">

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>

    @include('admin.products.create')
    @hasPermission('product_edit')
    <div id="modal-form-update" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body set-modal">
                    <div class="text-center mt-2 mb-4">
                        <h2 class="page-title text-danger">Edit Product</h2>
                        <hr>
                    </div>
                    <div id="edit">

                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endhasPermission

    @hasPermission('product_show')
    <div id="modal-form-show" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body set-modal">
                    <div class="text-center mt-2 mb-4">
                        <h2 class="page-title text-danger">Detail Product</h2>
                        <hr>
                    </div>
                    <div id="show">

                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endhasPermission
@endsection
@push('scripts')
    <script src="{{ asset("js/product.js") }}"></script>
@endpush
