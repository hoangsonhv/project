@extends("admin.layouts.layouts")
@section("content")
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">User</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-2">
                                @hasPermission('user_create')
                                <a href="{{ route("users.create") }}">
                                    <button type="button" class="btn btn-danger mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                        <i class="mdi mdi-plus-circle me-2"></i>Add User
                                    </button>
                                </a>
                                @endhasPermission
                            </div>
                            <div class=" col-md-10 set-search">
                                <form action="" method="get">
                                    <input name="name" class="form-control" value="{{ request()->name ?? '' }}" style="width: 20%" placeholder="Enter name...">
                                    <input name="email" class="form-control" style="width: 20%" value="{{ request()->email ?? '' }}" placeholder="Enter Email...">
                                    <select name="role_id" class="form-control" style="width: 10%; margin-right: 5px;margin-left: 5px">
                                        <option value="">None</option>
                                        @foreach($roles as $role)
                                            <option {{ $role->id == request('role_id') ? 'selected' : '' }} value="{{ $role->id }}">
                                                {{ $role->display_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                    <button type="submit" class="btn btn-primary" style="margin-left: 5px">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
                                <thead class="table-light">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th style="width: 85px;text-align: center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($users) != 0)
                                @foreach($users as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>
                                            <div class="container">
                                                <div class="flex-class justify-content-center">
                                                    <div class="col-sm-4">
                                                        <a href="{{ route('users.show', $item->id) }}"> <button type="button" class="btn btn-primary">
                                                                <i class="dripicons-information"></i>
                                                            </button>
                                                        </a>
                                                    </div>
                                                    @if($item->hasRoles('super-admin') == false)
                                                    @hasPermission('user_edit')
                                                        <div class="col-sm-4" >
                                                            <a href="{{ route('users.edit', $item->id) }}">
                                                                <button type="button" class="btn btn-secondary">
                                                                    <i class="dripicons-document-edit"></i>
                                                                </button>
                                                            </a>
                                                        </div>
                                                        @endhasPermission
                                                        @hasPermission('user_delete')
                                                            @if(auth()->user()->id != $item->id && $item->email != "admin@gmail.com")
                                                            <div class="col-sm-4">
                                                                <form id="delete-{{$item->phone}}-{{$item->id}}" action="{{ route('users.destroy',$item->id) }}" method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                                <button data-id="{{$item->id}}"
                                                                        data-name="{{ $item->phone }}"
                                                                        data-name-show="{{ $item->name }}"
                                                                        type="submit" class="btn btn-danger btn-delete">
                                                                    <i class="dripicons-trash"></i>
                                                                </button>
                                                            </div>
                                                            @endif
                                                        @endhasPermission
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                @else
                                    <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center"><span style="font-size: 25px; color: #d8d8d8">No data...</span></td>
                                    </tr>
                                    </tbody>
                                @endif
                            </table>
                            {{ $users->appends(request()->all())->links('vendor.pagination.bootstrap-5') }}
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
    @push('scripts')
        <script src="{{asset('js/user.js')}}"></script>
    @endpush
@endsection
