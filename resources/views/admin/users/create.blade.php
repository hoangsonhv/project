@extends('admin.layouts.layouts')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Create User</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('users.store') }}" data-action="{{ route('users.store') }}" id="form-add" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            @include('admin.layouts.form-errors')
                            <div class="mb-3">
                                <label>Name</label>
                                <input type="text" name="name" value="{{ old("name") }}" class="form-control" placeholder="Name">
                            </div>
                            <div class="mb-3">
                                <label>Email address</label>
                                <input type="email" name="email" value="{{ old("email") }}" class="form-control" placeholder="Email">
                            </div>
                            <div class="mb-3">
                                <label>Address</label>
                                <input type="text" name="address" value="{{ old("address") }}" class="form-control" placeholder="Address">
                            </div>

                            <div class="mb-3">
                                <label>Phone</label>
                                <input type="number" name="phone" value="{{ old("phone") }}" class="form-control" placeholder="Phone">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label"> Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" value="{{ old("password") }}" name="password" class="form-control" placeholder="Enter your password">
                                    <div class="input-group-text" data-password="false">
                                        <span class="password-eye"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                @if($roles->first() == null || auth()->user()->isSupperAdmin())
                                    <label>Role</label>
                                    <div class="checkbox select-all">
                                        <input id="all" type="checkbox" />
                                        <label for="all">Select all</label>
                                    </div>
                                    @foreach($roles as $role)
                                        <div class="checkbox rows col-md-4">
                                            <input id="box-{{ $role->id }}" class="select-item" name="role_id[]" value="{{ $role->id }}"
                                                   type="checkbox" {{ (is_array(old("role_id")) and in_array($role->id, old("role_id"))) ? 'checked' : '' }}/>
                                            <label for="box-{{ $role->id }}">{{ $role->display_name }}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a type="button" href="{{ route("users.index") }}" class="btn btn-secondary btn-button">Back</a>
                            <button type="submit" class="btn btn-primary btn-button">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

